import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  let strikeTime1 = 0, strikeTime2 = 0, interval;
  const strikePause = 10000;

  let leftHealthIndicator = 100, rightHealthIndicator = 100;
  const leftFighterIndicator = document.getElementById('left-fighter-indicator');
  const rightFighterIndicator = document.getElementById('right-fighter-indicator');
  
  let flag1 = true, flag2 = true;
  let downKeys = new Set();
  let {health: health1, attack: attack1 } = firstFighter;
  let {health: health2, attack: attack2 } = secondFighter;
  const leftStartHealth = health1;
  const rightStartHealth = health2;

  let prom = new Promise(resolve => {
  document.addEventListener('keydown', function (event) {
    downKeys.add(event.code);
    // critical hit left
    let leftAttack = controls.PlayerOneCriticalHitCombination
      .reduce((acc, it) => acc && downKeys.has(it), true);
    interval = Date.now() - strikeTime1;
    if (leftAttack && interval > strikePause) {
      strikeTime1 = Date.now();
      health2 -= 2 * attack1;
      controls.PlayerOneCriticalHitCombination.forEach(el => downKeys.delete(el));
    }
    // critical hit right
    let rightAttack = controls.PlayerTwoCriticalHitCombination
      .reduce((acc, it) => acc && downKeys.has(it), true);
    interval = Date.now() - strikeTime2;  
    if (rightAttack && interval > strikePause) {
      strikeTime2 = Date.now();
      health1 -= 2 * attack2;
      controls.PlayerTwoCriticalHitCombination.forEach(el => downKeys.delete(el));
    }

    

    // left attack
    let damage = 0;
    if(downKeys.has(controls.PlayerOneAttack) &&
      !downKeys.has(controls.PlayerOneBlock) &&
      flag1 === true) {
      flag1 = false;
      damage = getHitPower(firstFighter);
        if (downKeys.has(controls.PlayerTwoBlock)) {
          damage -= getBlockPower(secondFighter);
          downKeys.delete(controls.PlayerTwoBlock);
        }
        health2 = damage > 0 ? health2 - damage : health2;
        downKeys.delete(controls.PlayerOneAttack);
      }
    // right attack
    if(downKeys.has(controls.PlayerTwoAttack) &&
    !downKeys.has(controls.PlayerTwoBlock) &&
    flag2 === true) {
    flag2 = false;
    damage = getHitPower(secondFighter);
      if (downKeys.has(controls.PlayerOneBlock)) {
        damage -= getBlockPower(firstFighter);
        downKeys.delete(controls.PlayerOneBlock);        
      }
      health1 = damage > 0 ? health1 - damage : health1;
      downKeys.delete(controls.PlayerTwoAttack);
    }  
    // indicators
    if (health1 <= 0) {
      leftFighterIndicator.style.width = 0;
      resolve(secondFighter);  
    }
    if (health2 <= 0) {
      rightFighterIndicator.style.width = 0;
      resolve(firstFighter);  
    }
    leftHealthIndicator = (health1 / leftStartHealth) * 100;
    rightHealthIndicator = (health2 / rightStartHealth) * 100;
    leftFighterIndicator.style.width = leftHealthIndicator + '%';
    rightFighterIndicator.style.width = rightHealthIndicator + '%';
    
    document.addEventListener('keyup', function(event) {
      downKeys.delete(event.code);
      if (event.code === controls.PlayerOneAttack) flag1 = true;
      if (event.code === controls.PlayerTwoAttack) flag2 = true;
    });
  });
})
  let result = await prom;
  return new Promise(resolve => resolve(result));
  // return new Promise((resolve) => { 
  //   // resolve the promise with the winner when fight is over
  // });
}


export function getDamage(attacker, defender) {
let damage = getHitPower(attacker) - getBlockPower(defender);
return damage > 0 ? damage : 0;
  // return damage
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
  // return hit power
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
  // return block power
}