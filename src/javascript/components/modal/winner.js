import { showModal } from './modal';
import App from '../../app';

export function showWinnerModal(fighter) {
  const win = fighter.name.toUpperCase();
  const title = `The winner is    ${win}!`;
  let winner = {
    title,
    bodyElement: ` - Characteristics: - Health: ${fighter.health} - Attack: ${fighter.attack} - Defense: ${fighter.defense}`,
    onClose() {
      document.getElementById('root').innerHTML = '';
      new App();
    }
  };
  showModal(winner);
  // call showModal function 
}
