import { createElement } from '../helpers/domHelper';


export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) { 
    const {name, health, attack, defense} = fighter;
    const showProperties = createElement({
      tagName: 'div',
      className: 'white-text',
    });
    showProperties.innerText = `name: ${name} \nhealth:  ${health} \nattack:  ${attack} \ndefense: ${defense}`;
    const imgFighter = createFighterImage(fighter);
    fighterElement.prepend(imgFighter, showProperties);
  }
  // todo: show fighter info (image, name, health, etc.)
  //Done
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
